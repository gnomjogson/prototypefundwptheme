<?php
/*
 Template Name: Search Page
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php

//mobile detection
require_once 'library/php/Mobile_Detect.php';

include('partials/base-context.php');

//get GET parameter
if (isset($_GET['filter'])) {
    $filter = $_GET['filter'];
} else {
    $filter = '';
}
$filter = urlencode($filter);
$context['filter'] = $filter;

$context['search_term'] = get_search_query();
if(strlen($context['search_term']) > 0){
    $context['posts'] = new Timber\PostQuery();
} else $context['posts'] = [];

//get project categories
$args = array(
    'type' => 'project',
    'taxonomy' => 'projectcategory',
    'orderby' => 'name',
    'order'   => 'ASC'
);
$context['cats'] = get_categories($args);

//get categories
$context['blog_cats'] = get_categories();

Timber::render( 'views/page-search.twig', $context );
?>
