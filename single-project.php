<?php

//mobile detection
require_once 'library/php/Mobile_Detect.php';
require 'library/php/Embera/Autoload.php' ;

include('partials/base-context.php');

//$context['prev_project'] = get_adjacent_project($post->ID, 'prev');
//$context['next_project'] = get_adjacent_project($post->ID, 'next');


$values = get_field('status');
$field = get_field_object('status');

if(ICL_LANGUAGE_CODE == 'de'){
    $context['choices']['status1'] = 'In Arbeit';
    $context['choices']['status2'] = 'Im Einsatz';
    $context['choices']['status3'] = 'Abgeschlossen';
} else {
    $context['choices']['status1'] = 'In progress';
    $context['choices']['status2'] = 'In use';
    $context['choices']['status3'] = 'Completed';
}

$context['status'] = [];
$context['total_stati'] = 0;
if($context['choices']){
    foreach ($context['choices'] as $value => $label) {
        $array = [];
        $array['label'] = $label;
        if(is_array($values)){
            if (in_array($value, $values)) {
                $array['checked'] = 1;
                $context['total_stati']++;
            } else $array['checked'] = 0;
        }
        $context['status'][] = $array;
    }
}


$context['post'] = extendProjectPost($post);

Timber::render( 'views/single-project-2020.twig', $context );

?>