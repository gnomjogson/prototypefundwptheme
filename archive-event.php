<?php
/**
 * The template for displaying lists of events
 *
 * Queries to do with events will default to this template if a more appropriate template cannot be found
 *
 ***************** NOTICE: *****************
 *  Do not make changes to this file. Any changes made to this file
 * will be overwritten if the plug-in is updated.
 *
 * To overwrite this template with your own, make a copy of it (with the same name)
 * in your theme directory.
 *
 * WordPress will automatically prioritise the template in your theme directory.
 ***************** NOTICE: *****************
 *
 * @package Event Organiser (plug-in)
 * @since 1.0.0
 */

//mobile detection
require_once 'library/php/Mobile_Detect.php';
include('partials/base-context.php');

$context['eo_get_template_part'] = TimberHelper::function_wrapper( 'eo_get_template_part', array('eo-loop-events') );

Timber::render( 'views/archive-event.twig', $context );

?>
