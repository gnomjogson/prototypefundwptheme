<?php
/*
 Template Name: Projects Page 2020
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php

//mobile detection
require_once 'library/php/Mobile_Detect.php';

include('partials/base-context.php');

//get GET parameter
if (isset($_GET['filter'])) {
    $filter = $_GET['filter'];
} else {
    $filter = '';
}
$filter = urlencode($filter);
$context['filter'] = $filter;

//get projects
$round_slug_array = get_field('round',$post->ID);
$round_choices_array = get_field_object('round',$post->ID)['choices'];

$round_choices_array = [];
$round_choices_array['round_1'] = 'Round 1';
$round_choices_array['round_2'] = 'Round 2';
$round_choices_array['round_3'] = 'Round 3';
$round_choices_array['round_4'] = 'Round 4';
$round_choices_array['round_5'] = 'Round 5';
$round_choices_array['round_6'] = 'Round 6';
$round_choices_array['round_7'] = 'Round 7';
$round_choices_array['round_8'] = 'Round 8';
$round_choices_array['round_9'] = 'Round 9';
$round_choices_array['round_10'] = 'Round 10';
$round_choices_array['round_11'] = 'Round 11';
$round_choices_array['round_12'] = 'Round 12';
$round_choices_array['round_13'] = 'Round 13';
$round_choices_array['round_14'] = 'Round 14';
$round_choices_array['round_15'] = 'Round 15';
$round_choices_array['round_16'] = 'Round 16';
$round_choices_array['round_17'] = 'Round 17';
$round_choices_array['round_18'] = 'Round 18';
$round_choices_array['round_19'] = 'Round 19';
$round_choices_array['round_20'] = 'Round 20';
$round_choices_array['wirvsvirus'] = 'WirVsVirus';

if(!is_array($round_slug_array) ){
    $round_slug_array = explode(',',$round_slug_array);
    $round_choices_array = explode(',',$round_choices_array);
} else {
    if(count($round_slug_array) > 1){
        //overview
        $round_slug_array = [];
        $round_slug_array[] = 'round_1';
        $round_slug_array[] = 'round_2';
        $round_slug_array[] = 'round_3';
        $round_slug_array[] = 'round_4';
        $round_slug_array[] = 'round_5';
        $round_slug_array[] = 'round_6';
        $round_slug_array[] = 'round_7';
        $round_slug_array[] = 'round_8';
        $round_slug_array[] = 'round_9';
        $round_slug_array[] = 'round_10';
        $round_slug_array[] = 'round_11';
        $round_slug_array[] = 'round_12';
        $round_slug_array[] = 'round_13';
        $round_slug_array[] = 'round_14';
        $round_slug_array[] = 'round_15';
        $round_slug_array[] = 'round_16';
        $round_slug_array[] = 'round_17';
        $round_slug_array[] = 'round_18';
        $round_slug_array[] = 'round_19';
        $round_slug_array[] = 'round_20';
        $round_slug_array[] = 'wirvsvirus';
    }
}

$context['projects_rounds_filter'] = get_rounds($round_slug_array, $round_choices_array);
$context['projects'] = get_projects($round_slug_array);

if(count($round_slug_array) > 0){
    if(count($round_slug_array) < 2){
        //page is a single round page
        $context['hide_filters'] = true;
    } else {
        //add pagination
        $context['posts_per_page'] = 20;
        $context['pages_count'] = ceil(count($context['projects'])/$context['posts_per_page']);
        $context['show_pagination'] = true;
        $context['hide_filters'] = false;
    }
}

//get categories
$args = array(
    'type' => 'project',
    'taxonomy' => 'projectcategory',
    'orderby' => 'name',
    'order'   => 'ASC'
);
$context['cats'] = get_categories($args);

//activate the tiles, if on german page
if($context['options']['languageCode'] == 'de'){
    $context['projects_active'] = true;
}

//code page
$code_page_id = get_field('code_page_for_filter','option');
$content_post = get_post($code_page_id);
$content = $content_post->post_content;
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]&gt;', $content);
$context['code_page_content'] = $content;

Timber::render( 'views/page-projects-2020.twig', $context );
?>
