<?php
// ACF Gutenberg IHR custom blocks
add_action( 'acf/init', 'my_acf_init' );
function my_acf_init() {

    //google API key for maps
    acf_update_setting('google_api_key', 'AIzaSyDzlAq9_88G9pA6636D-x_TAmDr3kBkZic');

    // Bail out if function doesn’t exist.
    if ( ! function_exists( 'acf_register_block' ) ) {
        return;
    }

    // Custom Block Timeline
    acf_register_block( array(
        'name'            => 'customblock_timeline',
        'title'           => __( '⚪ The Timeline Block', 'rainbow-theme' ),
        'description'     => __( 'A block displaying the PTF timeline SVGs', 'rainbow-theme' ),
        'render_callback' => 'ru_acf_block_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'ptf', 'prototype fund', 'timeline'),
    ) );

    // Custom Block Datenvisualisierung
    acf_register_block( array(
        'name'            => 'customblock_dataviz',
        'title'           => __( '⚪ The Datavisualization Block', 'rainbow-theme' ),
        'description'     => __( 'A block displaying the PTF data viz', 'rainbow-theme' ),
        'render_callback' => 'ru_acf_block_render_callback',
        'category'        => 'formatting',
        'icon'            => 'admin-comments',
        'keywords'        => array( 'ptf', 'prototype fund', 'datavisualization'),
    ) );

}
/**
 *  This is the callback that displays the block.
 *
 * @param   array  $block      The block settings and attributes.
 * @param   string $content    The block content (emtpy string).
 * @param   bool   $is_preview True during AJAX preview.
 */
function ru_acf_block_render_callback( $block, $content = '', $is_preview = false ) {

    $context = Timber::get_context();

    // Store block values.
    $context['block'] = $block;

    // Store field values.
    $context['fields'] = get_fields();

    //preview or admin?
    $context['is_preview'] = $is_preview;
    $context['is_admin'] = is_admin();

    //store the slug
    $context['slug'] = str_replace('acf/', '', $block['name']);

    // include a template part from within the "templates/block" folder
    if( file_exists( get_theme_file_path("/views/blocks/block_{$context['slug']}.twig") ) ) {
        // Render the block.
        Timber::render( "views/blocks/block_{$context['slug']}.twig", $context );
    }
}