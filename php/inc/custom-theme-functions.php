<?php

/********* ◑____________◑ *********/

/* rainbow-unicorn.com
 * Custom theme functions go here keep the functions.php clean
 * ˛˛ƪ(⌾⃝ ౪ ⌾⃝ ๑)و ̉ ̉╰(▔∀▔)╯
ू(ʚ̴̶̷́ .̠ ʚ̴̶̷̥̀ ू)(━┳━｡ _ ｡━┳━)(ᵒ̤̑ ₀̑ ᵒ̤̑)wow!*✰
 */
/********* ◑____________◑ *********/

/*********************
diable Gutenberg Editor for certain custom post_type
 *********************/
function ru_disable_gutenberg( $current_status, $post_type ) {

    // Disabled post types
    $disabled_post_types = array( 'ct_projects' );

    // Change $can_edit to false for any post types in the disabled post types array
    if ( in_array( $post_type, $disabled_post_types, true ) ) {
        $current_status = false;
    }

    return $current_status;
}
add_filter( 'use_block_editor_for_post_type', 'ru_disable_gutenberg', 10, 2 );

/*********************
get categories by term slug
 *********************/
function ru_get_categories_by_term($term) {
    $cats = Timber::get_terms($term, array( 'hide_empty' => true));
    return $cats;
}

/*********************
get taxonomies by post_type
 *********************/
function ru_get_taxonomies_by_post_type($post_type) {
    $taxonomies = get_taxonomies( [ 'object_type' => [ $post_type ] ] );
    return $taxonomies;
}

/*********************
Get the ID of a page by the given menu item ID
 *********************/
function ru_get_page_id_by_menu_item_id ($id){
    return get_post_meta( $id, '_menu_item_object_id', true );
}

/*********************
parse a shortcode like [ru-template type=credits]
 *********************/
function ru_get_template( $atts ) {

    if(!empty($atts['type'])){
        ob_start();

        get_template_part( 'shortcode', $atts['type'] );
        return ob_get_clean();

    } else {
        return 'Err. Please specify markup to retrieve.';
    }

}
add_shortcode( "ru-template", "ru_get_template" );

/**
 * Like get_template_part() put lets you pass args to the template file
 * Args are available in the tempalte as $template_args array
 * @param string filepart
 * @param mixed wp_args style argument list
 */
function hm_get_template_part( $file, $template_args = array(), $cache_args = array() ) {

    $template_args = wp_parse_args( $template_args );
    $cache_args = wp_parse_args( $cache_args );
    if ( $cache_args ) {
        foreach ( $template_args as $key => $value ) {
            if ( is_scalar( $value ) || is_array( $value ) ) {
                $cache_args[$key] = $value;
            } else if ( is_object( $value ) && method_exists( $value, 'get_id' ) ) {
                $cache_args[$key] = call_user_method( 'get_id', $value );
            }
        }
        if ( ( $cache = wp_cache_get( $file, serialize( $cache_args ) ) ) !== false ) {
            if ( ! empty( $template_args['return'] ) )
                return $cache;
            echo $cache;
            return;
        }
    }
    $file_handle = $file;
    do_action( 'start_operation', 'hm_template_part::' . $file_handle );
    if ( file_exists( get_stylesheet_directory() . '/' . $file . '.php' ) )
        $file = get_stylesheet_directory() . '/' . $file . '.php';
    elseif ( file_exists( get_template_directory() . '/' . $file . '.php' ) )
        $file = get_template_directory() . '/' . $file . '.php';
    ob_start();
    $return = require( $file );
    $data = ob_get_clean();
    do_action( 'end_operation', 'hm_template_part::' . $file_handle );
    if ( $cache_args ) {
        wp_cache_set( $file, $data, serialize( $cache_args ), 3600 );
    }
    if ( ! empty( $template_args['return'] ) )
        if ( $return === false )
            return false;
        else
            return $data;
    echo $data;
}

/*********************
Just adds "..." to a provided $text after $limit chars
 *********************/
function ru_limit_text($text, $limit){
    $text = substr($text,0,$limit) . '...';
    return $text;
}

/**
 * Classic Gutenberg blocks get a default wrapper div
 *
 */
function ru_block_wrapper( $block_content, $block ) {
    //global $post;
    //$post_id = $post->ID;

    $wp_core_block = strpos($block['blockName'], 'core/');
    $index = strrpos($block['blockName'], '/', 0);
    $block_type = substr($block['blockName'],$index+1, strlen($block['blockName']));

    $innerHTML = $block['innerHTML'];
    $pos = strpos($innerHTML,">");
    $tag = substr($innerHTML, 2, $pos-2);
    if($tag == 'h1' || $tag == 'h2' || $tag == 'h3' || $tag == 'h4' || $tag == 'h5' || $tag == 'h6'){
        $block_type = $block_type . ' wp-' . $tag;
    }

    if($wp_core_block !== false) {
        $content = '<div class="col-24 wp-default-block wp-default-block__' . $block_type . '" data-element="' . $block_type . '" >';
        $content .= $block_content;
        $content .= '</div>';
        return $content;
    }
    return $block_content;
}
add_filter( 'render_block', 'ru_block_wrapper', 10, 2 );

/*********************
Add a custom class to Gutenberg editor page body
 *********************/
add_filter('admin_body_class', 'ru_admin_body_class');
function ru_admin_body_class($classes) {
    return $classes;
}

/**
 * Get ID of the first ACF block on the page
 */
function ru_get_first_block_id($id) {
    $post = get_post($id);
    if(isset($post->post_content)){
        if(has_blocks($post->post_content)) {
            $blocks = parse_blocks($post->post_content);
            $first_block_attrs = $blocks[0]['attrs'];
            if(array_key_exists('id', $first_block_attrs)) {
                return $first_block_attrs['id'];
            }
        }
    }

}

/*********************
Get type of the first ACF block on the page
 *********************/
function ru_get_first_block_type($id) {
    $post = get_post($id);
    if(isset($post->post_content)){
        if(has_blocks($post->post_content)) {
            $blocks = parse_blocks($post->post_content);
            $first_block_attrs = $blocks[0]['attrs'];
            if(array_key_exists('name', $first_block_attrs)) {
                return $first_block_attrs['name'];
            }
        }
    }
}

/*********************
Checks if a custom ACF Block is the first in editor or not
 *********************/
function ru_is_first_block($post_id, $block_id) {
    $first_block_id = ru_get_first_block_id($post_id);
    if($first_block_id == $block_id){
        return true;
    } else {
        return false;
    }
}

/*********************
get posts by category IDs
 *********************/
function ru_get_posts_by_category_ids($cats, $amount = -1, $orderby = 'menu_order', $order = 'ASC'){
    $cats = implode(',',$cats);

    $args = array(
        'post_type' => array('post','page'),
        'post_status' => 'publish',
        'cat' => $cats,
        'numberposts'       => $amount,
        'orderby' => $orderby,
        'order' => $order
    );

    if($orderby == 'date'){
        $args['order'] = 'DESC';
        $args['suppress_filters'] = true;
    }

    $posts = Timber::get_posts($args);
    return $posts;
}

/*********************
get posts by post_type
 *********************/
function ru_get_posts_by_post_type($post_type = 'post', $amount = -1, $orderby = 'menu_order', $order = 'ASC', $taxonomy_obj = false){

    $args = array(
        'post_type' => $post_type,
        'post_status' => 'publish',
        'posts_per_page' => $amount,
        'orderby' => $orderby,
        'order' => $order
    );

    //we have to get posts by custom taxonomy term_id
    if($taxonomy_obj){
        $args['tax_query'] = array(
            array(
                'taxonomy' => $taxonomy_obj->taxonomy,
                'field' => 'term_id',
                'terms' => $taxonomy_obj->term_id
            )
        );
    }

    if($orderby == 'date'){
        $args['order'] = 'DESC';
        $args['suppress_filters'] = true;
    }

    $posts = Timber::get_posts($args);
    return $posts;
}


/*
 *  Load Select Field `select_post_type` populated with the value and labels of the singular
 *  name of all public post types
 */
function ru_acf_load_post_types( $field ) {
    $choices = get_post_types( array( 'show_in_nav_menus' => true ), 'objects' );
    foreach ( $choices as $post_type ) :
        $field['choices'][$post_type->name] = $post_type->labels->singular_name;
    endforeach;
    return $field;
}
add_filter('acf/load_field/name=post_type_select', 'ru_acf_load_post_types');

/*********************
Get children of given page by id
 *********************/
function ru_timber_get_page_children( $page_id ) {

    $args = array(
        'child_of' => $page_id,
        'parent' => $page_id,
        'hierarchical' => 0,
        'sort_column' => 'menu_order',
        'sort_order' => 'asc'
    );
    $staff = get_pages( $args );
    $page_list = [];
    if($page_id != 0){
        foreach($staff as $s){
            $page_id = $s->ID;
            $post = Timber::get_post($page_id);
            $page_list[] = $post;
        }
        return $page_list;
    }

}

/*********************
get position of subpage from its parent id
 *********************/
function get_subpage_position_index($post_id) {
    $position_index = 1;
    $parent_id = wp_get_post_parent_id($post_id);

    if($parent_id != 0){
        $args = array(
            'child_of' => $parent_id,
            'parent' => $parent_id,
            'hierarchical' => 0,
            'sort_column' => 'menu_order',
            'sort_order' => 'asc'
        );
        $staff = get_pages( $args );

        $index = 1;
        foreach($staff as $s){
            if($s->ID == $post_id){
                $position_index = $index;
            }
            $index++;
        }
    }

    return $position_index;
}

/*********************
get position of subpage from its parent id
 *********************/
function ru_get_post_content_snippet($post_id, $words = 150, $suffix = '...') {
    $post = get_post($post_id);
    $snipped = '';
    $snippet = wp_trim_words( $post->post_content, $words ) . $suffix;
    return $snippet;
}

/*********************
Returns the file name from a given $file_name
 *********************/
function ru_get_filetype_from_string($file_name){
    $temp= explode('.',$file_name);
    $extension = end($temp);
    return strtoupper($extension);
}

/*********************
Returns the file name from a given $url
 *********************/
function ru_get_filename_from_url($url){
    $path = parse_url($url, PHP_URL_PATH);
    return basename($path);
}

/*********************
check if a timber template exists
 *********************/
function ru_timber_template_exists($template) {
    $loader = new Timber\Loader;
    if ( $loader->get_loader()->exists( $template ) ) {
        return true;
    } else return false;
}

/*********************
get orientation of image by id
 *********************/
function get_image_orientation($id) {
    $meta = wp_get_attachment_metadata($id);
    $orientation = "square";
    if(is_array($meta)){
        if($meta['width'] / $meta['height'] > 1) {
            $orientation = 'landscape';
        } else if($meta['width'] / $meta['height'] < 1) {
            $orientation = 'portrait';
        }
    }
    return $orientation;
}

/*********************
REMOVE OUTER <p></p> from given string
 *********************/
function ru_strip_outer_paragraph($string){
    $string = preg_replace('/<p[^>]*>(.*)<\/p[^>]*>/i', '$1', $string);
    return trim($string);
}

/*********************
 * Get a list of tag names in the provided HTML string
 * @return Array
 *********************/
function ru_get_all_tag_names($html){
    $tags = array();
    $part = explode("<", $html);
    foreach($part as $tag){
        $chunk = explode(" ", $tag);
        if(empty($chunk[0]) || $chunk[0][0] == "/") continue;
        $tag = trim($chunk[0], " >");
        if(!in_array($tag, $tags)) $tags[] = $tag;
    }
    return $tags;
}

/*********************
 * Strip only certain tags in the given HTML string
 * @param $html string
 * @param $tags [array] tags to remove
 * @return String
 *********************/
function ru_strip_tags_by_tag_names($html, $tags){
    $existing_tags = ru_get_all_tag_names($html);
    $allowable_tags = '<'.implode('><', array_diff($existing_tags, $tags)).'>';
    return strip_tags($html, $allowable_tags);
}

/*********************
return css classes for recurring classes in tempaltes
*********************/
function ru_get_css_classes($type){
    switch($type)
    {

        case 'section_title_margins':
            return 'mb-5 mb-md-5 mb-lg-6 mb-xxl-6 px-4 px-md-0 lh-1-2';
            break;
        default:
            return '';
    }
}

/*********************
returns svg from file
 *********************/
function ru_get_svg($filename) {
    $path = get_stylesheet_directory_uri() . '/library/img/svgs/' .$filename . '.svg';
    return file_get_contents($path);
}

/*********************
removes media upload button from custom post_type wysiwyg editor
*********************/
function ru_check_post_type_and_remove_media_buttons() {
    global $current_screen;
    // Replace following array items with your own custom post types
    $post_types = array('ct_xxx');
    if (in_array($current_screen->post_type,$post_types)) {
        remove_action('media_buttons', 'media_buttons');
    }
}
add_action('admin_head','ru_check_post_type_and_remove_media_buttons');


/*********************
Add a unique ID to custom blocks
 *********************/
add_filter(
    'acf/pre_save_block',
    function( $attributes ) {
        if ( empty( $attributes['id'] ) ) {
            $attributes['id'] = 'block_acf-block-' . uniqid();
        }
        return $attributes;
    }
);

