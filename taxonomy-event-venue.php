<?php
/**
 * The template for displaying the venue page
 *
 ***************** NOTICE: *****************
 *  Do not make changes to this file. Any changes made to this file
 * will be overwritten if the plug-in is updated.
 *
 * To overwrite this template with your own, make a copy of it (with the same name)
 * in your theme directory. See http://docs.wp-event-organiser.com/theme-integration for more information
 *
 * WordPress will automatically prioritise the template in your theme directory.
 ***************** NOTICE: *****************
 *
 * @package Event Organiser (plug-in)
 * @since 1.0.0
 */

//mobile detection
require_once 'library/php/Mobile_Detect.php';
require 'library/php/Embera/Autoload.php';

include('partials/base-context.php');

$venue_id = get_queried_object_id();

$context['eo_get_venue_name'] = TimberHelper::function_wrapper( 'eo_get_venue_name', array($venue_id) );
$context['venue_description'] = eo_get_venue_description( $venue_id );
$context['eo_venue_has_latlng'] = TimberHelper::function_wrapper( 'eo_venue_has_latlng', array($venue_id) );
$context['eo_get_template_part'] = TimberHelper::function_wrapper( 'eo_get_template_part', array('eo-loop-events') );
$context['eo_get_venue_map'] = eo_get_venue_map( $venue_id, array( 'width' => '100%' ) );

Timber::render( 'views/taxonomy-event-venue.twig', $context );

?>