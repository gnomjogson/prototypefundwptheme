<?php

//mobile detection
require_once 'library/php/Mobile_Detect.php';

include('partials/base-context.php');

$context['title'] = 'Search results for ' . get_search_query();
$context['posts'] = new Timber\PostQuery();

Timber::render( 'views/page-search.twig', $context );
?>