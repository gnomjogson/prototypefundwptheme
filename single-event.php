<?php

//mobile detection
require_once 'library/php/Mobile_Detect.php';
require 'library/php/Embera/Autoload.php' ;

include('partials/base-context.php');

$last_modified = $post->post_modified;
$context['time_passed'] = timePassedSinceDate(strtotime($last_modified));

$url = get_field('video_embed_url');
$embera = new \Embera\Embera();
$context['video_url'] = $embera->autoEmbed($url);

Timber::render( 'views/single.twig', $context );

?>

