<?php


$context = Timber::get_context();

//when on main projects page we need to include MAP js/css in html-header.twig
if(is_object($post)){
    $round_slug_array = get_field('round',$post->ID);
    if(is_array($round_slug_array)){
        if(count($round_slug_array) >= 2){
            $context['showMap'] = true;
        }
    }
}

$post = new TimberPost();
$context['post'] = $post;

//here we check what assets are needed on specific pages, based on post_id

//news
if($post->id == 35 || $post->id == 37 || $post->post_type == 'post'){
    $context['show_social_bar_in_header'] = true;
}



?>