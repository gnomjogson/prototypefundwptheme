<?php

//mobile detection
require_once 'library/php/Mobile_Detect.php';

include('partials/base-context.php');

//get GET parameter
if (isset($_GET['filter'])) {
    $filter = $_GET['filter'];
} else {
    $filter = '';
}
$filter = urlencode($filter);
$context['filter'] = $filter;

$term = get_queried_object();
$context['term_slug'] = $term->slug;

$context['posts'] = new Timber\PostQuery();

//get categories
$args = array(
    'type' => 'project',
    'taxonomy' => 'projectcategory',
    'orderby' => 'name',
    'order'   => 'ASC'
);
$context['cats'] = get_categories($args);

//get categories
$context['blog_cats'] = get_categories();
$context['archive_search'] = true;

Timber::render( 'views/page-search.twig', $context );

?>