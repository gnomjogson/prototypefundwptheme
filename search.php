<?php
/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

?>

<?php

//mobile detection
require_once 'library/php/Mobile_Detect.php';

include('partials/base-context.php');

//get GET parameter
if (isset($_GET['filter'])) {
    $filter = $_GET['filter'];
} else {
    $filter = '';
}
$filter = urlencode($filter);
$context['filter'] = $filter;

$context['search_term'] = get_search_query();
if(strlen($context['search_term']) > 0){
    $context['posts'] = new Timber\PostQuery();
} else {
    $context['posts'] = [];
}

//get categories
$args = array(
    'type' => 'project',
    'taxonomy' => 'projectcategory',
    'orderby' => 'name',
    'order'   => 'ASC'
);
$context['cats'] = get_categories($args);

//get categories
$context['blog_cats'] = get_categories();

Timber::render( 'views/page-search.twig', $context );

?>
