const gulp = require('gulp');
const babel = require('gulp-babel');
const scss = require('gulp-sass')(require('node-sass'));
const del = require('del');
const prefix = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const cleanCSS = require('gulp-clean-css');
const minify = require('gulp-minify');
const babelify = require('babelify');
const concat = require('gulp-concat');
const browserify = require('browserify');
const util = require('gulp-util');
const source = require('vinyl-source-buffer');
const buffer = require('vinyl-buffer');
const rename = require('gulp-rename');

const mode = require('gulp-mode')( {
    modes: ["production", "development"],
    default: "development",
    verbose: false
});
const path = {
    scss: './library/scss/',
    css: './library/css/',
    js: './library/js/',
    dist: './dist/'
};

gulp.task('clean', () => {
    return del([
        path.css + 'style.css',
    ]);
});

gulp.task('scss', () => {
    return gulp.src( path.scss + '/**/*.scss')
        .pipe(mode.development(sourcemaps.init()))
        .pipe(scss().on('error', scss.logError))
        .pipe(prefix('last 2 versions'))
        .pipe(mode.development(sourcemaps.write('../maps')))
        .pipe(gulp.dest(path.css));
});

gulp.task('minify-css',() => {
    return gulp.src(path.css + '*.css')
        .pipe(mode.production(cleanCSS({level: 2})))
        .pipe(gulp.dest(path.css));
});

gulp.task('clean', () => {
    return del([
        path.css + 'style.css',
        path.dist,
    ]);
});

/*
gulp.task('scripts', function() {
    var b = browserify({
        entries: path.js + 'scripts.js',
        debug: true,
        transform: [babelify.configure({
            presets: ['es2015']
        })]
    });

    return b.bundle()
        .pipe(source(path.js + 'scripts.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        // Add other gulp transformations (eg. uglify) to the pipeline here.
        .on('error', util.log)
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest(path.dist));
});
*/

/*
gulp.task('minify-js', function() {
    return gulp.src(path.js + 'scripts.js')
        .pipe(minify({
            ext: {
                min: '.min.js'
            },
            ignoreFiles: ['-min.js']
        }))
        .pipe(gulp.dest(path.dist))
});*/

gulp.task('watch', () => {
    gulp.watch([path.scss + '**/*.scss', path.js + '**/*.js'], {ignoreInitial: false}, (done) => {
        gulp.series(['clean', 'scss', 'minify-css'])(done);
    });
});

gulp.task('default', gulp.series(['clean', 'scss', 'minify-css', 'watch']));
